# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 10:33:49 2020

@file BLE_main.py
@brief A file for managing finite state machines that modulate the frequency of 
an LED using bluetooth
@author Patrick Ward

@image html BLE_TaskDia.PNG

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab5/BLE_main.py
"""
import pyb
from BLEdriver import BLEdriver

## Bluetooth UART and LED Object
BLE = BLEdriver(3,9600,pyb.Pin.cpu.A5)

from Blink import BlinkTask
from BLEUI import TaskUI

## Task object 1
task1 = TaskUI(BLE)

## Taks object 2
task2 = BlinkTask(BLE) # Will also run constructor
# To run the task call task1.run() over and over
while True: #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()
#    task3.run()