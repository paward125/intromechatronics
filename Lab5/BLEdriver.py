# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 15:19:41 2020

@file BLEdriver.py
@brief A file for creating a Bluetooth UART and LED driver
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab5/BLEdriver.py
"""

import pyb
from pyb import UART

class BLEdriver:
    ''' This class implements LED and UART objects on the ME305 board to be 
    controlled and communicated with through Bluetooth.'''
    
    def __init__ (self, uartNum, uartBaud, LEDpin):
        ''' Creates a UART object and an LED object that can be use with Bluetooth communication
        @param uartNum a value that identifies which UART is being used on the NUCLEO
        @param uartBaud sets the baudrate of the UART in use
        @param LEDpin A pyb.Pin object to control the LED'''
        
        ## Establishes the UART attribute of the class object
        self.uart = UART(uartNum, uartBaud)
        
        ## Establishes the LED pin attribut of the class object
        self.pin = pyb.Pin (LEDpin, pyb.Pin.OUT_PP)
        
    def write (self, string):
        '''
        Writes a given imput into the serial port
        '''
        self.uart.write(string)
        
    def read (self):
        '''
        Reads all characters in the serial port
        '''
        return self.uart.read()
    
    def ANY (self):
        '''
        Checks for any characters in the serial port
        '''
        return self.uart.any()
    
    def on (self):
        '''
        Turns the LED on
        '''
        self.pin.high()
        
    def off (self):
        '''
        Turns the LED off
        '''
        self.pin.low()
        
        
        