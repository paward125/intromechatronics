# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 11:41:19 2020

@file BLEUI.py
@brief A finite state machine for recieving input from a user and handling that data
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab5/BLEUI.py
"""

import Shared

class TaskUI:
    '''
    An input recieving task.
    
    A Bluetooth UART object continously checks for user input. The input is 
    checked to ensure that it fits the range from 1-10. This number is used to
    set the interval of the Blink.py task. For example an input of 4 will cause
    the LED in Blink.py to blink at 4 hertz.
    
    @image html BLEUIFnStDiag.PNG
    
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT          = 0
    
    ## State 1 - Checking for input and handeling that input
    S1_RECEIVING     = 1
    
    
    def __init__(self, BLE):
        '''
        Creates an input recieving task object.
        @param BLE An object that is a Bluetooth UART class
        '''
        ## The Encoder object
        self.BLE = BLE
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        
        if (self.state == self.S0_INIT):
            # Run State 0 Code
            self.transitionTo(self.S1_RECEIVING)
                
        elif (self.state == self.S1_RECEIVING):
            if self.BLE.ANY() != 0:
                inpt = self.BLE.read()
                inpt = int(inpt)
                if inpt in [1,2,3,4,5,6,7,8,9,10]:
                    Shared.inter = inpt
                    self.BLE.write('LED blinking at ' + str(inpt) + ' Hz')
                else:
                    self.BLE.write('Choose a number between 1 and 10')
        else:
                # Uh-oh state (undefined sate)
                # Error handling
            pass
            
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState