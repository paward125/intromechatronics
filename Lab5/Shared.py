# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 10:43:38 2020

@file Shared.py
@brief A file containing all the shared variables in Lab05
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab5/Shared.py
"""

## The interval value sent from the BLEUI.py task to the Blink.py task
inter = int(0)
