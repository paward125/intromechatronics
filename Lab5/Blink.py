# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 15:54:06 2020

@file Blink.py
@brief A finite state machine for blinking an LED
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab5/Blink.py
"""

import utime
import Shared

class BlinkTask:
    '''
    An LED blinking task.
    
    An LED object continously blinks on and off.
    
    @image html BlinkFnStDiag.PNG
    
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT           = 0
    
    ## State 1 - LED on
    S1_ON             = 1
    
    ## State 2 - LED off
    S2_OFF            = 2
    
    def __init__(self, BLE):
        '''
        Creates an LED task object.
        @param BLE An object that can function as an LED object
        '''
        ## The Encoder object
        self.BLE = BLE
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # Time stamp for task starting
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int(5e5)
        
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        
        ## Stores the length of time that the program has been running
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if (self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_ON)
                
            elif (self.state == self.S1_ON):
                if Shared.inter in [1,2,3,4,5,6,7,8,9,10]:
                    self.interval = int(5e5/Shared.inter)
                    
                self.BLE.on()
                self.transitionTo(self.S2_OFF)
                
            elif (self.state == self.S2_OFF):
                if Shared.inter in [1,2,3,4,5,6,7,8,9,10]:
                    self.interval = int(5e5/Shared.inter)
                    
                self.BLE.off()
                self.transitionTo(self.S1_ON)
                        
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState