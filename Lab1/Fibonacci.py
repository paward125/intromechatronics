# -*- coding: utf-8 -*-
"""
This file allows for a user to find a Fibonacci number

The file passes a parameter idx through the function 
fib to calculate the Fibonacci number corresponding 
to the index value of idx.

The source code is found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab1/Fibonacci.py

@author Patrick Ward

@file Fibonacci.py

@date September 30, 2020
"""

def fib (idx):
    ''' This method calculates a Fibonacci number corresponding to
    a specified index. It uses a bottom up method for determining the 
    Fibonacci number. Specifically, the code uses if, elif, and else 
    statements to consider which portion of the squence to consider. 
    In the else section there is a for loop to add numbers in a squence 
    until reaching the requested index value.
    
    @param idx An integer specifying the index of the desired
            Fibonacci number.'''
            
    print ('Calculating Fibonacci number at '
           'index n = {:}.'.format(idx))
    fib0 = 0 # this is the zero index value of the Fibonacci sequence
    fib1 = 1 # this is the first index value of the Fibonacci sequence
    fibn = 0 # This variable is used to store any value greater than index 1
    
    if (idx < 0):
        print ('Incorrect value. Please choose a positive integer when you choose to find another number.')
    elif (idx < 2):
        fibn = idx
        print ('The Fibonacci number for index %(idx)d is %(fibn)d' % {"idx": idx, "fibn": fibn})
    else:
        for x in range(1, idx):
            fibn = fib0 + fib1
            fib0 = fib1
            fib1 = fibn
        print('The Fibonacci number for index %(idx)d is %(fibn)d' % {"idx": idx, "fibn": fibn})
        
    answer = str(input('Do you want to find another number? Enter yes or no:'))
    if answer in ['y', 'Y', 'yes', 'Yes', 'YES']:
        idx = int(input('Enter the index that you are interested in:'))
        fib(idx)
    else:
        pass

if __name__ == '__main__':
    # Replace the following statement with the user interface code
    # that will allow testing of your Fibonnaci function. Any code
    # within the if __name__ == '__main__' block will only run when
    # the script is executed as a standalone program. If the script
    # is imported as a module the code block will not run.
    fib(-5)
