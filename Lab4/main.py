# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 12:57:22 2020

@file main.py
@brief A file for managing finite state machines
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab4/main.py
"""
import pyb

from Encoder import Encoder
Encoder1 = Encoder(pyb.Pin.cpu.A6, pyb.Pin.cpu.A7, 3)

from Task import TaskData
task1 = TaskData(Encoder1,0.2)

while True: 
    task1.run()
