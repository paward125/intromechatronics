# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 14:19:19 2020

@file Task.py
@brief A task for collecting encoder data
@author Patrick Ward

Source code can be found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab4/Task.py
"""

import utime
from pyb import UART

class TaskData:
    '''
    A data collecting task.
    
    An encoder object continously updates its position at a perscribed interval.
    It also checks for user input into the user interface task recieved through
    serial communication. If it recieves a g the data collecting begins. If it 
    an s the data collecting will stop.The data is sent using serial communication.
    After 10 seconds has passed the data collection will stop without an s command
    needing to be recieved.
    
    @image html TaskData.PNG
    
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    S1_NO_DATA              = 1
    
    S2_DATA_COLLECT         = 2
    
    def __init__(self, Encoder, interval):
        '''
        Creates a data collecting task object.
        @param interval An integer number of seconds between desired runs of the task
        @param Encoder An object that is an encoder class
        '''
        ## The Encoder object
        self.Encoder = Encoder
        
        ## Sets up the UART for the Nucleo
        self.myuart = UART(2)
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # Time stamp for task starting
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int(interval*1e6)
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The variable used to create a referenc time for when data collecting begins
        self.dataTime = 0
        
        ## To store time data
        self.t = []
        
        ## To store position data
        self.p = []
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if (self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_NO_DATA)
                
            elif (self.state == self.S1_NO_DATA):
                
                if self.myuart.any() != 0:
                    val = self.myuart.readchar()
                    if val in [71,103]:
                        self.transitionTo(self.S2_DATA_COLLECT)
                        self.dataTime = utime.ticks_us()
                    else:
                        self.transitionTo(self.S1_NO_DATA)
                
            elif (self.state == self.S2_DATA_COLLECT):
                self.Encoder.update()
                
                if self.myuart.any() != 0:
                    val = self.myuart.readchar()
                
                    if val in [83, 115]: 
                        self.transitionTo(self.S1_NO_DATA)
                        self.myuart.write('dn')
                        
                elif (utime.ticks_diff(utime.ticks_us(),self.dataTime) > (10.1*1e6)):
                    self.transitionTo(self.S1_NO_DATA)
                    self.myuart.write('dn')
                        
                else:
                    self.transitionTo(self.S2_DATA_COLLECT)
                    self.t = utime.ticks_diff(utime.ticks_us(), self.dataTime)
                    self.p = self.Encoder.position
                    #print('{:},{:}\r\n'.format(self.p,self.t))
                    self.myuart.write('{:},{:}\r\n'.format(self.p,self.t))
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState