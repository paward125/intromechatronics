# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 12:59:51 2020

@file User.py
@brief A file for recording data from an encoder
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab4/User.py
"""

import serial
import time
import matplotlib.pyplot as plt
import numpy as np

ser = serial.Serial(port='COM3',baudrate=115273,timeout=20)
print('Enter G to begin recording. Once you are finished recording data enter s')
print('The data collection will run for 10 seconds unless s is entered')
Char = False;

while Char == False:
    dataStart = input('Enter command:')
    if (dataStart =='G' or dataStart =='g'):
        ser.write(str(dataStart).encode('ascii'))
        Char = True
    else:
        print('Please enter G if you would like to begin collecting data')

timeStart = time.time() 

while (Char == True and time.time()<(timeStart+10)):
    dataStop = input('The program has began collecting data. Enter an S to stop collecting data or the program will stop collecting after 10 seconds')
    if (dataStop == 'S' or dataStop =='s'):
        ser.write(str(dataStop).encode('ascii'))
        Char = False
    elif (time.time()>=(timeStart+10)):
        print('The data collection has terminated because 10 seconds have passed')
    else:
        print('Incorrect command')

t = []
p = []
line_string = []

while True:
    line_string = ser.readline().decode()					# read a single line of text
    line_list = line_string.strip().split(',')		# strip any special characters and then split the string into wherever a comma appears;
    if line_list != ['dn']:
        t.append(int(line_list[1])/1e6)
        p.append(int(line_list[0])*(360/1400))
    else:
        break
ser.close()

plt.plot(t,p)
plt.ylabel('Angular Position [Degrees]')
plt.xlabel('Time [s]')
plt.show()

np.savetxt('DegVsTim.csv',[t,p],delimiter=",")





#def sendChar():
#    inv = input('Give me a character: ')
#    ser.write(str(inv).encode('ascii'))
#     myval = ser.readline().decode('ascii')
#     return myval

# for n in range(1):
#     print(sendChar())
#     ser.close()
