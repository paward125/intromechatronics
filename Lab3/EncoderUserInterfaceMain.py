'''
@file EncoderUserInterfaceMain.py

This is an operative file for running multiple tasks on a nucleo

This task managing file is particullarly concerned with an encoder and a user interface

You can find the source code here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab3/EncoderUserInterfaceMain.py

@author Patrick Ward

@date October 8, 2020
'''

from Encoder import Encoder
import pyb

## Encoder driver object
Encoder1 = Encoder(pyb.Pin.cpu.A6, pyb.Pin.cpu.A7, 3)

from TaskEncoder import TaskEncoder
from TaskUser import TaskUser
## Task object for updating the encoder
task1 = TaskEncoder(Encoder1,1,True,1) # Will also run constructor

## Task object for User interface
task2 = TaskUser(1,True,2)
# To run the task call task1.run() over and over
while True: #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()
#    task3.run()
