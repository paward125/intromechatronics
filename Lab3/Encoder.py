# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 13:52:34 2020

@file Encoder.py
@brief An encoder class
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab3/Encoder.py
"""
import pyb

class Encoder:
    '''
    @brief This class is for setting up an encoder object and defining all the functions
    needed to interact with the encoder.
    
    This class utilizes a timer on a nucleo as a counter for an encoder for a motor.
    This class accounts for overflow and underflow of the timer counter.
    
    '''
    def __init__(self, pin1, pin2, timer):
        '''
        Creates an encoder object.
        @param timer an encoder object that allows choice in timer on the nucleo
        @param pin1 an encoder object that allows timer ch1 choice
        @param pin2 an encoder object that allows timer ch2 choice
        '''
        self.timer = timer
        
        self.pin1 = pin1
        
        self.pin2 = pin2
        
        self.tim = pyb.Timer(self.timer)
        self.tim.init(prescaler=0, period=0xFFFF)
        self.tim.channel(1, pin=self.pin1, mode=pyb.Timer.ENC_AB)
        self.tim.channel(2, pin=self.pin2, mode=pyb.Timer.ENC_AB)
        
        self.tim.counter()
        
        self.position = 0
        
        self.nextPos = 0
        
        self.delta = 0
    def get_position(self):
        '''
        Gets the next position to compare to the last recorded position
        '''
        self.nextPos = self.tim.counter()
        
    def get_delta(self):
        '''
        Gets the change in position 
        '''
        self.get_position()
        self.delta = self.nextPos - self.position
    
    def update(self):
        '''
        Updates the position
        '''
        self.get_delta()
        if (self.delta > 0):
            if (self.delta<(0xFFFF)*0.5):
                self.position = self.position + self.delta
            else:
                self.delta = self.delta-0xFFFF
                self.position = self.position + self.delta
        else:
            if(self.delta>(0xFFFF)*-0.5):
                self.position = self.position + self.delta
            else:
                self.delta = self.delta + 0xFFFF
                self.position = self.position + self.delta
       
    def set_position(self):
        '''
        Resets the timer and position of the motor
        '''
        
        self.position = 0
        
        self.nextPos = 0
        
        self.delta = 0
        
        self.tim.counter(0)
        