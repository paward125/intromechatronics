'''
@file shares.py
@brief A container for all the inter-task variables
@author Patrick Ward

The source code can be found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab3/shares.py
'''

## The command character sent from the user interface task to the cipher task
cmd     = None
