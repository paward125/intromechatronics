# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 17:26:41 2020

@file TaskEncoder.py
@brief An encoder task class
@author Patrick Ward

Source code can be found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab3/TaskEncoder.py
"""

# from Encoder.py import Encoder
import utime
import shares

class TaskEncoder:
    '''
    An Encoder task.
    
    An encoder object continously updates its position at a perscribed interval.
    It also checks for user input into the user interface task. If it recieves a 
    correct letter value then various commands are carried out.
    
    @image html TaskEncoder.PNG
    
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    S1_UPDATE               = 1
    
    def __init__(self, Encoder, interval, dbg, taskNum):
        '''
        Creates an encoder task object.
        @param taskNum A number to identify the task
        @param interval An integer number of seconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        @param Encoder An object that is an encoder class
        '''
        
        self.taskNum = taskNum
        
        self.Encoder = Encoder
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # Time stamp for task starting
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int(interval*1e6)
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        self.dbg = dbg
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if (self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_UPDATE)
                self.printTrace()
                
            elif (self.state == self.S1_UPDATE):
                self.transitionTo(self.S1_UPDATE)
                self.Encoder.update()
                
                if (shares.cmd):
                    self.respondToUser()
                    shares.cmd = None
                    
                self.printTrace()
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
            
    def respondToUser(self):
        '''
        Checks that the keyboard command is one available and then executes it
        '''
        if shares.cmd in [90, 122]:
            self.Encoder.set_position()
            print(str('Position has been set to zero'))
            
        elif shares.cmd in [80, 112]:
            print(str(self.Encoder.position))
            
        elif shares.cmd in [68, 100]:
            print(str(self.Encoder.delta))
            
        else:
            print(str('This character does not have a function'))
            pass
