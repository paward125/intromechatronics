# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 10:49:11 2020

@file TaskUser.py
@brief A user interface task
@author Patrick Ward

Source code can be found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab3/TaskUser.py
"""

import shares
import utime
from pyb import UART

class TaskUser:
    '''
    User interface task.
    
    An object of this class interacts with serial port UART2 waiting for 
    characters. Each character received is checked and if it is an english
    letter a-z it is sent through a cipher in TaskCipher.
    
    @image html TaskUser.PNG
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_WAIT_FOR_CHAR    = 1
    

    def __init__(self, interval, dbg, taskNum):
        '''
        Creates a user interface task object.
        @param taskNum A number to identify the task
        @param interval An integer number of seconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        
        if self.dbg:
            print('Created user interface task')

        print('Keyboard commands include: z setting the current motor position to zero, p returns the current position, d returns the most recent delta between updates')
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
            
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                self.printTrace()
                # Run State 1 Code
                if self.ser.any():
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)
                    shares.cmd = self.ser.readchar()
                    
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)