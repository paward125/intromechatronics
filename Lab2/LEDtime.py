'''
@file LEDtime.py

This file serves as an example implementation of a finite-state-machine using
microPython. The example will implement some code to control a LED's intensity
as well as control an imaginary LED turing it on and off.

You can find the source code here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab2/LEDtime.py

@author Patrick Ward

@date October 14, 2020
'''
import utime
import pyb


class TaskLEDblink:
    '''
    @brief      A finite state machine to control an imaginary LED.
    @details    This class implements a finite state machine to control the
                operation of an LED.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    ## Constant defining State 1
    S1_LED_ON               = 1
    
    ## Constant defining State 2
    S2_LED_OFF              = 2
    
    def __init__(self, interval):
        '''
        @brief      Creates a TaskLEDblink object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # Time stamp for task starting
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int(interval*1e6)
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S2_LED_OFF)
                print(str(self.runs) + ' LED OFF {:0.2f}'.format(utime.ticks_diff(self.curr_time, self.start_time)))
            
            elif(self.state == self.S1_LED_ON):
                # Run State 1 Code
                self.transitionTo(self.S2_LED_OFF)
                print(str(self.runs) + ' LED OFF {:0.2f}'.format(utime.ticks_diff(self.curr_time, self.start_time)))
            
            elif(self.state == self.S2_LED_OFF):
                # Run State 2 Code
                self.transitionTo(self.S1_LED_ON)
                print(str(self.runs) + ' LED ON {:0.2f}'.format(utime.ticks_diff(self.curr_time, self.start_time)))
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState  

class TaskLEDteeth:
    '''
    @brief      A finite state machine to control an LED.
    @details    This class implements a finite state machine to control the
                operation of an LED so that it ramps in intensity and then drops
                back to low to create a tooth pattern.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    ## Constant defining State 1
    S1_PRINT_VAL            = 1
    
    def __init__(self, interval, period):
        '''
        @brief      Creates a TaskLEDteeth object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int(interval*1e6)
        
        ## The interval of time, in seconds, between runs of the task
        self.period = int(period*1e6)
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time,self.interval)
        
        ## A counter showing the number of times the task has run
        self.runs = 0
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us() #updating the current timestamp
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_PRINT_VAL)
                pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
                pinA5.low ()
            
            elif(self.state == self.S1_PRINT_VAL):
                # Run State 1 Code
                pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
                tim2 = pyb.Timer(2, freq = 20000)
                t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
                t2ch1.pulse_width_percent((((self.interval*self.runs)/self.period)*100))
                if (int(self.interval*self.runs) >= self.period):
                    self.transitionTo(self.S0_INIT)
                    self.runs = -1
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState 