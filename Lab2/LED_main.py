# -*- coding: utf-8 -*-
'''
@file LED_main.py

This is an operative file for running multiple tasks on a nucleo

You can find the source code here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab2/LED_main.py

@author Patrick Ward

@date October 8, 2020
'''

from LEDtime import TaskLEDblink, TaskLEDteeth

## Task object
task1 = TaskLEDblink(1) # Will also run constructor
task2 = TaskLEDteeth(1,10)
# To run the task call task1.run() over and over
while True: #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()
#    task3.run()