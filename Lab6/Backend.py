# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 17:05:55 2020

@file Backend.py
@brief A finite state machine for receiving proportional control values and for
sending back time and speed data.
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab5/Backend.py
"""

import utime
from pyb import UART
import share

class TaskData:
    '''
    A data receiving and sending task.
    
    A serial object continously checks for input from the user for a proportional controller 
    value. Once the motor has run and the data is recorded in ControllerTask.py
    the Kp equal to none is triggered allowing for the finite state machine to send over
    the motor speed and time data through the serial port.
    
    @image html Backend.PNG
    
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    ## Waiting State 1 - Waits for a Kp value to be input
    S1_WAITING              = 1
    
    ## Sending State 2 - Sends all the recorded data from ControllerTask.py through the serial port
    S2_SENDING              = 2
    
    def __init__(self, interval, taskNum, dbg):
        '''
        Creates a data collecting task object.
        @param interval An integer number of seconds between desired runs of the task
        @param taskNum Denotes which task for debugging
        @param dbg Turns on or off debugging functionality
        '''
        
        ## Sets up the UART for the Nucleo
        self.myuart = UART(2)
        
        ## Logical value for debugging on or off
        self.dbg = dbg
        
        ## Denotes which task for debugging
        self.taskNum = taskNum
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # Time stamp for task starting
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int(interval*1e6)
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The number of runs
        self.runs = 1
        
        ## The indexing variable for sending the data
        self.count = 0
        
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The current timestamp
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if (self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAITING)
                
                self.printTrace()
                
            elif (self.state == self.S1_WAITING):
                self.printTrace()
                if self.myuart.any() != 0:
                    share.Kp = float(self.myuart.readline())
                    #print(share.Kp)
                    self.transitionTo(self.S2_SENDING)
                
                #share.Kp = 0.3
                #self.transitionTo(self.S2_SENDING)
                
                
            elif (self.state == self.S2_SENDING):
                self.printTrace()
                if share.Kp == None:
                    leng = len(share.speed)
                    
                    if self.count < leng:
                        #self.myuart.write('{:},{:},{:}\r\n'.format(share.speed[self.count],share.time[self.count],share.L[self.count]))
                        #self.myuart.write('{:},{:},{:}\r\n'.format(share.speed[self.count],share.time[self.count],share.p[self.count]))
                        self.myuart.write('{:},{:}\r\n'.format(share.speed[self.count],share.time[self.count]))
                        self.count += 1
                    else:
                        self.myuart.write('dn')
                        self.transitionTo(self.S1_WAITING)
                        self.count = 0
                
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
    
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)