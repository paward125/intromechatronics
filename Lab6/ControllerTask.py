# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 10:32:14 2020

@file ControllerTask.py
@brief A finite state machine class that controls and records data from a motor.
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab5/ControllerTask.py
"""
import array
import utime
import share

class ControllerTask:
    '''
    A motor controlling task and motor data collection task.
    
    This class first waits for a proportional controller value. After recieving the Kp 
    value the finite state machine begins running the moter and recording the motor data.
    After 3 seconds have passed all variables are reset and the finite state machine goes back
    to waiting for an input Kp.
    
    @image html ControllerTask.PNG
    
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
   
    ## Waiting State 1 - Waits for a Kp value
    S1_WAITING          = 1
    
    ## Update State 1 - Updates the duty value of the motor and records speed data
    S2_UPDATE           = 2
    
    ## Zero State 1 - Resets all the variables in preperation for a new Kp value
    S3_ZERO             = 3
    
    def __init__(self, ENC, MOE, clsdlp, interval, taskNum, dbg):
        '''
        Creates a data collecting task object.
        @param interval An integer number of seconds between desired runs of the task
        @param ENC An object that is an encoder class
        @param MOE A motor driver object
        @param clsdlp A feedback loop junction object
        @param taskNum Differentiates between multiple tasks
        @param dbg A logical value that turns on and off debugging capabilities
        '''
        ## The Encoder object
        self.ENC = ENC
        
        ## A debugging variable that allows the modulation of debugging capabilities
        self.dbg = dbg
        
        ## A variable that differentiates this finite state machine from others
        self.taskNum = taskNum
        
        ## The motor driver object
        self.MOE = MOE
        
        ## The feedback junction object
        self.clsdlp = clsdlp
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # Time stamp for task starting
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int(interval*1e6)
        
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Records the number of runs the finites state machine has made
        self.runs = 1
        
        ## A variable used for storing single speed data points
        self.omeg = 0
        
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## Contains the current time stamp
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if (self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAITING)
                self.printTrace()
                
            elif (self.state == self.S1_WAITING):
                if share.Kp != None:
                    self.transitionTo(self.S2_UPDATE)
                    self.clsdlp.set_Kp(share.Kp)
                    share.speed = array.array('f', [])
                    share.time = array.array('f', [])
                    share.L = array.array('f', [])
                    share.p = array.array('f', [])
                    self.rec_time = utime.ticks_us()
                    self.ENC.update()
                    self.MOE.enable()
                
                self.printTrace()
                
            elif (self.state == self.S2_UPDATE):
                self.ENC.update()
                #share.p.append(self.ENC.position)
                delt = self.ENC.get_delta()
                delt = delt*(1/4000)
                
                self.omeg = (delt/self.interval)*1e6*60
                
                stopT =utime.ticks_diff(self.curr_time, self.rec_time)
                
                share.speed.append(self.omeg)
                share.time.append(stopT)
                
                L = self.clsdlp.update(self.omeg)
                #print(str(L) + 'this is L')
                #share.L.append(L)
                self.MOE.set_duty(L)
                
                if stopT >= 3e6:
                    self.transitionTo(self.S3_ZERO)
                    
                self.printTrace()
                
            elif (self.state == self.S3_ZERO):
                share.Kp = None
                self.omeg = 0
                self.MOE.set_duty(0)
                self.MOE.disable()
                self.ENC.set_position()
                self.transitionTo(self.S1_WAITING)
                
                self.printTrace()
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
            
            
