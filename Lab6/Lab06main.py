# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 10:42:44 2020

@file Lab06main.py
@brief A file that operates as the control center for multiple finite state machines
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab6/Lab06main.py

Below in the first figure is the task diagram
Following the task diagram I have three plots showing the progression of Kp values from 0.03, 0.035, and 0.04
@image html Lab6Taskdia.PNG
@image html plot1.png
@image html plot2.png
@image html plot3.png
"""

import pyb

from Encoder import Encoder
## An Encoder driver object
Encoder1 = Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)

from MotorDriver import MotorDriver
## A Motor driver object
MOE = MotorDriver(pyb.Pin.cpu.A15, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 3, 1, 2)

## debugging variable
dbg = False
## Contains the interval that the ControllerTask runs at
interval = 0.02

from ClosedLoop import ClosedLoop
## The feedback loop junction object
clsdlp = ClosedLoop()

from ControllerTask import ControllerTask
## Contains the Controller Task object
task2 = ControllerTask(Encoder1, MOE, clsdlp, interval, 2, dbg)

from Backend import TaskData
## Contains the Backend task object
task1 = TaskData(0.1, 1, dbg)

while True: 
    task1.run()
    task2.run()