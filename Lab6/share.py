# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 10:39:40 2020

@file share.py
@brief A file that contains all of the variables passed between Backend.py and ControllerTask.py
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab6/share.py
 
"""

## Proportional controller value
Kp = None

## Holds the speed data
speed = None

## Holds the time data
time = None

## Holds the reference speed data
ref = 1600

## Holds the duty values sent to the motor
L = None

## Holds the positional data of the motor
p = None