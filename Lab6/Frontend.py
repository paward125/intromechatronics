# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 18:01:53 2020

@file Frontend.py
@brief A file that operates as the user interface for motor controll
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab6/Frontend.py
"""

import serial
import matplotlib.pyplot as plt
import numpy as np
import time

## This is a serial object
ser = serial.Serial(port='COM3',baudrate=115273,timeout=20)
print('Enter a Kp to start the motor and begin measurements.')
print('The data collection will run for 10 seconds.')

## Stores the input value for Kp
dataStart = (input('Enter Kp:'))
ser.write(str(dataStart).encode('ascii'))

## Contains an initiation time stamp
timeStart = time.time()

while time.time()<(timeStart+10):
    pass

## Contains time data
t = []
## Contains speed data
th_dot = []

#L = []
## Contains data read from the serial
line_string = []
#p = []

while True:
    line_string = ser.readline().decode()					# read a single line of text
    ## Contains modified data from the serial
    line_list = line_string.strip().split(',')		# strip any special characters and then split the string into wherever a comma appears;
    if line_list != ['dn']:
        t.append(float(line_list[1])/1e6)
        th_dot.append(float(line_list[0]))
        #L.append(float(line_list[2]))
        #p.append(float(line_list[2]))
    else:
        break
ser.close()

plt.plot(t,th_dot)
plt.ylabel('Angular Velocity [rpm]')
plt.xlabel('Time [s]')
#plt.axis([0, 3, 0, 3000])
plt.show()

#plt.plot(t,p)
#plt.show()

#plt.plot(t,L)
#plt.show()


np.savetxt('DegVsTim.csv',[t,th_dot],delimiter=",")