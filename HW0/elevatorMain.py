'''
@file elevatorMain.py

This is an operative file for running multiple tasks 

You can find the source code here: https://bitbucket.org/paward125/intromechatronics/src/master/HW0/elevatorMain.py

@author Patrick Ward

@date October 4, 2020
'''

from Cooperative_Multitasking import Button, MotorDriver, TaskElevator
        
## Motor Object
Motor_a = MotorDriver()

## Button Object for first floor selection
Button_1_a = Button('PA6')

## Button Object for second floor selection
Button_2_a = Button('PA7')

## Button Object for bottom limit
First_a = Button('PA8')

## Button Object for upper limit
Second_a = Button('PA9')

## Task object
task1 = TaskElevator(0.1, Motor_a, Button_1_a, Button_2_a, First_a, Second_a) # Will also run constructor

## Motor Object
Motor_b = MotorDriver()

## Button Object for first floor selection
Button_1_b = Button('PB6')

## Button Object for second floor selection
Button_2_b = Button('PB7')

## Button Object for bottom limit
First_b = Button('PB8')

## Button Object for upper limit
Second_b = Button('PB9')

## Task object
task2 = TaskElevator(0.2, Motor_b, Button_1_b, Button_2_b, First_b, Second_b) # Will also run constructor

# To run the task call task1.run() over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()
#    task3.run()
