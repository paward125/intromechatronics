# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 13:03:19 2020

@file MotorDriver.py
@brief A file that contains a motor driver class
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab6/MotorDriver.py
 
"""

import pyb

class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''

    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer, channel1, channel2):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on
        IN1_pin and IN2_pin.
        @param channel1 contains channel value for IN1_pin
        @param channel2 contains channel value for IN2_pin'''
        
        #print ('Creating a motor driver')
        
        #self.nSLEEP_pin = nSLEEP_pin()
        
        #self.IN1_pin = IN1_pin()
        
        #self.IN2_pin = IN2_pin()
        
        #self.timer = timer()
        
        ## Contains a sleep pin object
        self.pinSLEEP = pyb.Pin(nSLEEP_pin,pyb.Pin.OUT_PP)
        self.pinSLEEP.low()
        
        ## Contains a pyb.Pin object to use as the input to half bridge 1
        self.pin1 = pyb.Pin(IN1_pin)
        
        ## Contains a pyb.Pin object to use as the input to half bridge 1
        self.pin2 = pyb.Pin(IN2_pin)
        
        ## Contains a timer object
        self.tim = pyb.Timer(timer,freq=20000)
        
        ## Contains timer channel1 object
        self.tch1 = self.tim.channel(channel1,pyb.Timer.PWM,pin=self.pin1)
        
        ## Contains timer channel2 object 
        self.tch2 = self.tim.channel(channel2,pyb.Timer.PWM,pin=self.pin2)
        self.tch1.pulse_width_percent(0)
        self.tch2.pulse_width_percent(0)

    def enable (self):
        '''
        Enables the motor
        '''
        #print ('Enabling Motor')
        self.pinSLEEP.high()

    def disable (self):
        '''
        Disables the motor
        '''
        #print ('Disabling Motor')
        self.pinSLEEP.low()

    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
        
        self.tch1.pulse_width_percent(0)
        self.tch2.pulse_width_percent(0)
        if duty > 100:
            self.tch1.pulse_width_percent(100)
        elif duty < -100:
            self.tch2.pulse_width_percent(100)
        elif duty < 0:
            duty = abs(duty)
            self.tch2.pulse_width_percent(duty)
        elif duty > 0:
            self.tch1.pulse_width_percent(duty)
        elif duty == 0:
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(0)
        else:
            
            pass

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.

    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP = pyb.Pin.cpu.A15
    pin_IN1 = pyb.Pin.cpu.B4
    pin_IN2 = pyb.Pin.cpu.B5

    # Create the timer object used for PWM generation
    tim = 3

    channel1 = 1
    
    channel2 = 2    

    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim, channel1, channel2)
    
    # Enable the motor driver
    moe.enable()

    # Set the duty cycle to 10 percent
    moe.set_duty(0)