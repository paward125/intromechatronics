# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 10:42:44 2020

@file Lab7_main.py
@brief A file that operates as the control center for multiple finite state machines
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab7/Lab7_main.py

In this Lab I was not able to get my prgrams to run correctly there was an issue 
when I was sending over my resampled data for the reference speed profile. I could
not correctly figure out how to get the bytearray sent from the laptop to be converted
to a float array. I sent a value at the end of the speed profile 
data to flag the transition to the Kp accepting state. From there on the code is very similar
to before except I am recording a positional array in addition to the speed array.
Also, I made it so that there is an iterative counter included for my Lab7_ControllerTask.py
to iterate through each reference speed. The recorded data is written to the Laptop side
and is processed as it was in previous labs. After this I used my data from the motor
and compared it to the reference data given to us to determine the accuracy of my
controller. In addition there is code in my Lab7_Frontend.py that would create the
plots necessary for comparing the motor data to the reference data.

Below in the first figure is the task diagram
@image html Lab6Taskdia.PNG
"""

import pyb

from Encoder import Encoder
## An Encoder driver object
Encoder1 = Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)

from MotorDriver import MotorDriver
## A Motor driver object
MOE = MotorDriver(pyb.Pin.cpu.A15, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 3, 1, 2)

## debugging variable
dbg = False
## Contains the interval that the ControllerTask runs at
interval = 0.02

from Lab7_ClosedLoop import ClosedLoop
## The feedback loop junction object
clsdlp = ClosedLoop()

from Lab7_ControllerTask import ControllerTask
## Contains the Controller Task object
task2 = ControllerTask(Encoder1, MOE, clsdlp, interval, 2, dbg)

from Lab7_Backend import TaskData
## Contains the Backend task object
task1 = TaskData(0.001, 1, dbg)

while True: 
    task1.run()
    task2.run()