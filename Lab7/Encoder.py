# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 13:52:34 2020

@file Encoder.py
@brief An encoder class
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab6/Encoder.py
"""
import pyb

class Encoder:
    '''
    @brief This class is for setting up an encoder object and defining all the functions
    needed to interact with the encoder.
    
    This class utilizes a timer on a nucleo as a counter for an encoder for a motor.
    This class accounts for overflow and underflow of the timer counter.
    
    '''
    def __init__(self, pin1, pin2, timer):
        '''
        Creates an encoder object.
        @param timer an encoder object that allows choice in timer on the nucleo
        @param pin1 an encoder object that allows timer ch1 choice
        @param pin2 an encoder object that allows timer ch2 choice
        '''
        
        ## This variable contains the timer callout
        self.timer = timer
        
        ## This pin contains the channel 1 pin callout
        self.pin1 = pin1
        
        ## This pin contains the channel 2 pin callout
        self.pin2 = pin2
        
        ## Creates a timer object
        self.tim = pyb.Timer(self.timer)
        self.tim.init(prescaler=0, period=0xFFFF)
        self.tim.channel(1, pin=self.pin1, mode=pyb.Timer.ENC_AB)
        self.tim.channel(2, pin=self.pin2, mode=pyb.Timer.ENC_AB)
        
        self.tim.counter()
        
        ## initiates the encoder position
        self.position = 0
        
        ## Contains a reference value for generating deltas
        self.lastPos = 0
        
        ## Contains another reference value for generating deltas
        self.nextPos = 0
        
        ## Contains delta values
        self.delta = 0
    def get_position(self):
        '''
        Gets the next position to compare to the last recorded position
        '''
        self.nextPos = self.tim.counter()
        
    def get_delta(self):
        '''
        Gets the change in position 
        '''
        return self.delta
    
    def update(self):
        '''
        Updates the position
        '''
        self.get_position()
        self.delta = self.nextPos - self.lastPos
        self.lastPos = self.nextPos
        if (self.delta >= 0):
            if (self.delta<=(0xFFFF)*0.5):
                self.position = self.position + self.delta
            else:
                self.delta = self.delta-0xFFFF-1
                self.position = self.position + self.delta
        else:
            if(self.delta>=(0xFFFF)*-0.5):
                self.position = self.position + self.delta
            else:
                self.delta = self.delta + 0xFFFF+1
                self.position = self.position + self.delta
       
    def set_position(self):
        '''
        Resets the timer and position of the motor
        '''
        self.lastPos = 0
        
        self.position = 0
        
        self.nextPos = 0
        
        self.delta = 0
        
        self.tim.counter(0)
    
        