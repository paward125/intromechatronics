# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 13:19:12 2020

@file Lab7_ClosedLoop.py
@brief A class that operates as the junction in a closed loop controller.
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab7/Lab7_ClosedLoop.py
"""
import shar
class ClosedLoop:
    ''' This class implements the feedback portion of a motor controller. It uses
    a reference speed that the controller trys to approach. It compares this refrence speed 
    to the measured output speed of the motor.'''

    def __init__ (self):
        ''' Creates a class that can use a reference speed and the feedback speed
        to output a new duty cycle percent. 
        '''
        ## The proportional controller value
        self.Kp = 0
        
    def update (self,speed, n):
        '''
        Updates the duty cycle value
        @param speed is the feedback speed from the motor
        '''
        L = (self.Kp)*(shar.ref[n]-speed)
        #L = self.Kp*(share.ref)
        return L
    
    def get_Kp (self):
        '''
        Returns the proportional controller value. 
        '''
        return self.Kp
    
    def set_Kp (self, Kp):
        '''
        Sets the proportional controller value.
        @param Kp is the new input proportional controller value
        '''
        self.Kp = Kp