# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 18:01:53 2020

@file Lab7_Frontend.py
@brief A file that operates as the user interface for motor control and data analysis
@author Patrick Ward

Source code found here: https://bitbucket.org/paward125/intromechatronics/src/master/Lab7/Lab7_Frontend.py
"""

import serial
import matplotlib.pyplot as plt
import numpy as np
import time

## This is a serial object
ser = serial.Serial(port='COM3',baudrate=115273,timeout=20)

# Blank lists for data. On the Nucleo you should probably use array.array() to
# better storage density. Make sure you pick the right data type when you init
# the array if you do go with array.array instead of a list.
## Contains time from the csv
tim = []
## Contains Angular velocity from the csv file
velocity = []
## Contains angular position from the csv file
position = []

# Open the file. From here it is essentially identical to serial comms. There
# are better ways to do this, like using the 'with' statement, but using an
# object like this is closest to the serial code you've already used.
## CSV object
ref = open('reference.csv');

# Read data indefinitely. Loop will break out when the file is done.
#
# You will need to handle this slightly differently on the Nucleo or
# pre-process the provided data, because there are 15,001 rows of data in the
# CSV which will be too much to store in RAM on the Nucleo.
#
# Consider resampling the data, interpolating the data, or manipulating the 
# data in some fashion to limit the number of rows based on the 'interval' for
# your control task. If your controller runs every 20ms you do not need the
# data sampled at 1ms provided by the file.
#
while True:
    # Read a line of data. It should be in the format 't,v,x\n' but when the
    # file runs out of lines the lines will return as empty strings, i.e. ''
    ## Contains the data from each line of CSV
    line = ref.readline()
    
    # If the line is empty, there are no more rows so exit the loop
    if line == '':
        break
    
    # If the line is not empty, strip special characters, split on commas, and
    # then append each value to its list.
    else:
        ## Contains the three values seperated from the csv file
        (t,v,x) = line.strip().split(',');
        tim.append(float(t))
        velocity.append(float(v))
        position.append(float(x))

#Can't forget to close the serial port
ref.close()

## To store angular velocity at the correct time interval
V = []
## Tos store angular position at the correct time interval
Pos = []

## Iterative counter
n = 0
while n <= 15001:
    ser.write(str(velocity[n]).encode('ascii'))
    V.append(velocity[n])
    Pos.append(position[n])
    n += 20

## A value sent to the Nucleo to cause a state transistion in Lab7_Backednd.py    
stateTran = 'dn'
ser.write(stateTran.encode('ascii'))


#timeStart1 = time.time()
# logic = True
# while logic:
#     feedbk = ser.readline().decode().strip()
#     print(feedbk)
#     if feedbk == 'transition':
#         logic = False
#     pass

print('Enter a Kp to start the motor and begin measurements.')
print('The data collection will run for 10 seconds.')

## Stores the input value for Kp
dataStart = (input('Enter Kp:'))
ser.write(str(dataStart).encode('ascii'))

## Contains an initiation time stamp
timeStart2 = time.time()

while time.time()<(timeStart2+20):
    pass

## Contains time data
t = []
## Contains speed data
th_dot = []

#L = []
## Contains data read from the serial
line_string = []
## Contains the positional data
p = []

while True:
    line_string = ser.readline().decode()					# read a single line of text
    ## Contains modified data from the serial
    line_list = line_string.strip().split(',')		# strip any special characters and then split the string into wherever a comma appears;
    if line_list != ['dn']:
        t.append(float(line_list[1])/1e6)
        th_dot.append(float(line_list[0]))
        #L.append(float(line_list[2]))
        p.append(float(line_list[2]))
    else:
        break
ser.close()


## Iterative counter
i = 0
## Length of output data
leng = len(t)
## The variable that store the accuracy value
J = 0
while i < leng:
    J += (((V[i]-th_dot[i])**2)+((Pos[i]-p[i])**2))
    i += 1

J = J/i
print(str(J))

plt.figure(1)

plt.subplot(2,1,1)
plt.plot(tim,velocity,t,th_dot)
plt.ylabel('Angular Velocity [rpm]')
plt.xlabel('Time [s]')

plt.subplot(2,1,2)
plt.plot(tim,position,t,p)
plt.xlabel('Time [s]')
plt.ylabel('Position [deg]')
#plt.axis([0, 3, 0, 3000])
plt.show()


#plt.plot(t,p)
#plt.show()

#plt.plot(t,L)
#plt.show()


np.savetxt('DegVsTim.csv',[t,th_dot],delimiter=",")